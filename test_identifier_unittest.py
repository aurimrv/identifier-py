from identifier import Identifier
import unittest

class TestIdentifier(unittest.TestCase):
    def setUp(self):
        self.identifier = Identifier()

    def test_valid_identifier_01(self):
        is_valid = self.identifier.validate_identifier('a')

        self.assertEqual(is_valid, True)


    def test_valid_identifier_02(self):
        is_valid = self.identifier.validate_identifier('')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_03(self):
        is_valid = self.identifier.validate_identifier('abcdefg')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_04(self):
        is_valid = self.identifier.validate_identifier('1')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_05(self):
        is_valid = self.identifier.validate_identifier('1')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_06(self):
        is_valid = self.identifier.validate_identifier('abcdef')

        self.assertEqual(is_valid, True)


    def test_valid_identifier_07(self):
        is_valid = self.identifier.validate_identifier(' A')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_08(self):
        is_valid = self.identifier.validate_identifier('123456')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_09(self):
        is_valid = self.identifier.validate_identifier('çabc')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_10(self):
        is_valid = self.identifier.validate_identifier('#çabc')

        self.assertEqual(is_valid, False)


    def test_valid_identifier_11(self):
        is_valid = self.identifier.validate_identifier('a b c')

        self.assertEqual(is_valid, False)

    def test_valid_identifier_12(self):
        is_valid = self.identifier.validate_identifier('abc#')

        self.assertEqual(is_valid, False)