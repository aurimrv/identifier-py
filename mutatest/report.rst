Mutatest diagnostic summary
===========================
 - Source location: /home/auri/temp/esp-web-ws/identifier-py/identifier.py
 - Test commands: ['python', '-m', 'unittest', 'test_identifier_unittest.py']
 - Mode: f
 - Excluded files: []
 - N locations input: 10
 - Random seed: None

Random sample details
---------------------
 - Total locations mutated: 10
 - Total locations identified: 37
 - Location sample coverage: 27.03 %


Running time details
--------------------
 - Clean trial 1 run time: 0:00:00.037419
 - Clean trial 2 run time: 0:00:00.037316
 - Mutation trials total run time: 0:00:01.268180

Overall mutation trial summary
==============================
 - DETECTED: 22
 - SURVIVED: 5
 - TIMEOUT: 1
 - TOTAL RUNS: 28
 - RUN DATETIME: 2022-06-13 22:31:18.811707


Mutations by result status
==========================


SURVIVED
--------
 - identifier.py: (l: 30, c: 36) - mutation from <class 'ast.LtE'> to <class 'ast.GtE'>
 - identifier.py: (l: 30, c: 36) - mutation from <class 'ast.LtE'> to <class 'ast.Lt'>
 - identifier.py: (l: 30, c: 36) - mutation from <class 'ast.LtE'> to <class 'ast.NotEq'>
 - identifier.py: (l: 30, c: 36) - mutation from <class 'ast.LtE'> to <class 'ast.Gt'>
 - identifier.py: (l: 30, c: 36) - mutation from <class 'ast.LtE'> to <class 'ast.Eq'>


TIMEOUT
-------
 - identifier.py: (l: 14, c: 20) - mutation from <class 'ast.Add'> to <class 'ast.Pow'>


DETECTED
--------
 - identifier.py: (l: 7, c: 11) - mutation from <class 'ast.Gt'> to <class 'ast.Eq'>
 - identifier.py: (l: 7, c: 11) - mutation from <class 'ast.Gt'> to <class 'ast.NotEq'>
 - identifier.py: (l: 7, c: 11) - mutation from <class 'ast.Gt'> to <class 'ast.Lt'>
 - identifier.py: (l: 7, c: 11) - mutation from <class 'ast.Gt'> to <class 'ast.LtE'>
 - identifier.py: (l: 7, c: 11) - mutation from <class 'ast.Gt'> to <class 'ast.GtE'>
 - identifier.py: (l: 14, c: 20) - mutation from <class 'ast.Add'> to <class 'ast.Sub'>
 - identifier.py: (l: 14, c: 20) - mutation from <class 'ast.Add'> to <class 'ast.Div'>
 - identifier.py: (l: 16, c: 11) - mutation from <class 'ast.And'> to <class 'ast.Or'>
 - identifier.py: (l: 16, c: 49) - mutation from <class 'ast.LtE'> to <class 'ast.NotEq'>
 - identifier.py: (l: 16, c: 49) - mutation from <class 'ast.LtE'> to <class 'ast.GtE'>
 - identifier.py: (l: 16, c: 49) - mutation from <class 'ast.LtE'> to <class 'ast.Lt'>
 - identifier.py: (l: 16, c: 49) - mutation from <class 'ast.LtE'> to <class 'ast.Eq'>
 - identifier.py: (l: 16, c: 49) - mutation from <class 'ast.LtE'> to <class 'ast.Gt'>
 - identifier.py: (l: 17, c: 19) - mutation from True to None
 - identifier.py: (l: 17, c: 19) - mutation from True to False
 - identifier.py: (l: 19, c: 19) - mutation from False to True
 - identifier.py: (l: 19, c: 19) - mutation from False to None
 - identifier.py: (l: 22, c: 8) - mutation from If_Statement to If_True
 - identifier.py: (l: 22, c: 8) - mutation from If_Statement to If_False
 - identifier.py: (l: 22, c: 11) - mutation from <class 'ast.Or'> to <class 'ast.And'>
 - identifier.py: (l: 23, c: 19) - mutation from True to False
 - identifier.py: (l: 23, c: 19) - mutation from True to None