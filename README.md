# Instruções para Execução de Diferentes Ferramentas de Teste para Python

O programa principal está no módulo <code>identifier.py</code> e dentro desse módulo está implementada a classe <code>Identifier</code> com os métodos: <code>validate_identifier</code>, <code>valid_s</code> e <code>valid_f</code>.

Além disso, há um conjunto de teste denominado <code>test_identifier_pytest.py</code> e <code>test_identifier_unittest.py</code> que são utilizados pra ilustrar a execução dos testes considerando esses dois *frameworks* para automatização de execução de testes unitários em Python.

Os comandos abaixo foram redigidos considerando a estrutura deste projeto mas podem ser facilmente adaptado para outros projetos.

Neste arquivo são listados comandos para executar diferentes ferramentas de teste:

- <code>pytest</code> e <code>unittest</code>: *frameworks* para se escrever testes unitários automatizados para Python

- <code>coverage</code>: que mede a cobertura de código dos testes acima

- <code>mut.py</code>, <code>mutmut</code>, <code>mutatest</code> e <code>cosmic-ray</code>: ferramentas que apoiam a aplicação do teste de mutação. 

- <code>pynguin</code>: gerador de dados de teste

## 1) Executando casos de testes unitários:

### PyTest

```
python -m pytest -v test_identifier_pytest.py
```

### PyTest Único Teste

```
python -m pytest -v test_identifier_pytest.py::test_valid_identifier_01
```

### UnitTest

```
python -m unittest -v test_identifier_unittest.py
```

### UnitTest Único Teste

```
python -m unittest -v test_identifier_unittest.TestIdentifier.test_valid_identifier_01
```

## 2) [coverage](https://coverage.readthedocs.io/en/latest/)

Medindo a cobertura dos testes unitários

### PyTest

```
coverage run --source=identifier --branch -m pytest test_identifier_pytest.py 
```

### UintTest

```	
coverage run --source=identifier --branch -m unittest test_identifier_unittest.py 
```

```
coverage report

coverage html -d coverage
```

## 3) [mutpy](https://github.com/mutpy/mutpy)

Observe nos comandos abaixo a presença do comando <code>time</code> do Linux. Tal comando pode ser omitido e está presente simplemente para viabilizar a coleta do tempo de execução do respectivo comando.

### PyTest

```
time mut.py --debug -c -e -t identifier.py --runner pytest -u test_identifier_pytest.py -m --report-html mutpy
```

### UnitTest

```
time mut.py --debug -c -e -t identifier.py --runner unittest -u test_identifier_unittest.py -m --report-html mutpy
```

## 4) [mutmut](https://mutmut.readthedocs.io/en/latest/)

### PyTest

```
time mutmut run --paths-to-mutate identifier.py --runner 'python -m pytest test_identifier_pytest.py'
```

### UnitTest

```
time mutmut run --paths-to-mutate identifier.py --runner 'python -m unittest test_identifier_unittest.py'
```

```
mutmut html

mv html mutmut
```

```
mutmut results

mutmut apply --backup 5 # build mutant source code
```

## 5) [mutatest](https://mutatest.readthedocs.io/en/latest/index.html)

### PyTest

```
time mutatest --mode f -s identifier.py -t "python -m pytest test_identifier_pytest.py" -m f -o mutatest/report.rst
```

### UnitTest

```
time mutatest --mode f -s identifier.py -t "python -m unittest test_identifier_unittest.py" -m f -o mutatest/report.rst
```

## 6) [cosmic-ray](https://cosmic-ray.readthedocs.io/en/latest/index.html)

### 6.1) Configurando sessao de teste

#### PyTest

```
cosmic-ray new-config cosmic-cfg-pytest.toml

    [?] Top-level module path: identifier.py
    [?] Test execution timeout (seconds): 5
    [?] Test command: pytest -m pytest -v test_identifier_pytest.py
    -- MENU: Distributor --
      (0) http
      (1) local
    [?] Enter menu selection: 1
```

#### UnitTest

```
	cosmic-ray new-config cosmic-cfg-unittest.toml

    [?] Top-level module path: identifier.py
    [?] Test execution timeout (seconds): 5
    [?] Test command: pytest -m pytest -v test_identifier_unittest.py
    -- MENU: Distributor --
      (0) http
      (1) local
    [?] Enter menu selection: 1
```


### 6.2) Iniciando banco de dados

#### PyTest

```
cosmic-ray --verbosity=INFO init cosmic-cfg-pytest.toml cosmic-bd-pytest.sqlite
```

#### UnitTest

```
cosmic-ray --verbosity=INFO init cosmic-cfg-unittest.toml cosmic-bd-unittest.sqlite
```

### 6.3) Definindo baseline (verificando se os testes passam no código original)

#### PyTest

```
cosmic-ray --verbosity=INFO baseline cosmic-cfg-pytest.toml 

cr-report cosmic-bd-pytest.sqlite --show-pending
```

#### UnitTest

```
cosmic-ray --verbosity=INFO baseline cosmic-cfg-unittest.toml

cr-report cosmic-bd-unittest.sqlite --show-pending
```

### 6.4) Executando os mutantes

#### PyTest

```
cosmic-ray --verbosity=INFO exec cosmic-cfg-pytest.toml cosmic-bd-pytest.sqlite
```

#### UnitTest

```
cosmic-ray --verbosity=INFO exec cosmic-cfg-unittest.toml cosmic-bd-unittest.sqlite
```

### 6.5) Gerando Relatórios

#### PyTest

```
mkdir cosmic-ray

cr-html cosmic-bd-pytest.sqlite > cosmic-ray/report-pytest.html
```

#### UnitTest

```
mkdir cosmic-ray

cr-html cosmic-bd-unittest.sqlite > cosmic-ray/report-unittest.html
```

### 6.6) Descobrindo estrutura do banco de dados da Cosmic-Ray

```
*********************************
Consultando Tabela Cosmic-Ray
sqlite3 
.help
.open cosmic-bd-unittest.sqlite
.databases
.fullschema
CREATE TABLE work_items (
	job_id VARCHAR NOT NULL, 
	PRIMARY KEY (job_id)
);
CREATE TABLE mutation_specs (
	module_path VARCHAR, 
	operator_name VARCHAR, 
	occurrence INTEGER, 
	start_pos_row INTEGER, 
	start_pos_col INTEGER, 
	end_pos_row INTEGER, 
	end_pos_col INTEGER, 
	job_id VARCHAR NOT NULL, 
	PRIMARY KEY (job_id), 
	FOREIGN KEY(job_id) REFERENCES work_items (job_id)
);
CREATE TABLE work_results (
	worker_outcome VARCHAR(9), 
	output TEXT, 
	test_outcome VARCHAR(11), 
	diff TEXT, 
	job_id VARCHAR NOT NULL, 
	PRIMARY KEY (job_id), 
	FOREIGN KEY(job_id) REFERENCES work_items (job_id)
);
/* No STAT tables available */
```

## 7) [pynguin](https://pynguin.readthedocs.io/en/latest/)

Esta ferramenta é uma das poucas que existem para oferecer suporte à geração automática de dados de teste para Python. A seguir são ilustrados três comandos sobre a execução dessa ferramenta.

Por padrão, o critério de parada estabelecido para o gerador é atingir 600 segundos de execuão. Para dentificar o número de iterações e o tempo de execução máximos para um determinado projeto pode ser utilizado o comando abaixo:

```
pynguin --project-path . --output-path pynguin-results --module-name identifier -v --seed 1 --coverage-metrics BRANCH

```

Sem critérios de parada definidos, a ferramenta usa seus valores padrão máximos: 600 segundos.

### 7.1) Critério de parada com base no número de iterações

Gera os testes parando após 10 iterações

```
pynguin --project-path . --output-path pynguin-results --module-name identifier -v --seed 1 --coverage-metrics BRANCH --stopping_condition MAX_ITERATIONS --maximum-iterations 10
```

### 7.2) Critério de parada com base no tempo em segundos

Gera os testes parando após 10 segundos

```
pynguin --project-path . --output-path pynguin-results --module-name identifier -v --seed 1 --coverage-metrics BRANCH --stopping_condition MAX_TIME --budget 10
```

Última atualização: 17/06/2022